// 3. Create an addStudent() function that will accept a name of the student and add it to the student array.


let students = [];

function addStudent(student){
	let studentsLength = students.push(student);
	console.log(student + " was added to the student's list");
}

// 4. Create a countStudents() function that will print the total number of students in the array.
function countStudents(){
	let numStudents = students.reduce(function(x,y){
		return x + 1;
	}, 0);
	console.log("There are a total of " + numStudents + " students enrolled.")
}

// Create a printStudents() function that will sort and individually print the students (sort and forEach methods) in the array.

function printStudents(){
	students.sort();
	students.forEach(function(student){
		console.log(student);
	})
}

function findStudent(name){
	let result = students.filter(function(student){
		return student.toLowerCase().includes(name.toLowerCase());
	})
	if(result.length >= 2){
		console.log(result.toString() + " are enrollees");
	}
	else if(result.length == 1){
		console.log(name + " is an enrollee");
	}
	else{
		console.log('No student found with the name ' + name)
	}
}